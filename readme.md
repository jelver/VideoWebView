### **原生WebView+WebChromeClient，对于HTML5的Video标签支持很不友好**  
[测试apk下载地址](http://http://git.oschina.net/yso/VideoWebView/attach_files/download?i=17974&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FB5%2FfMqNk1ZYTimAd8v5ABA8nrb6YDg276.apk%3Ftoken%3D03713f485cb836a9760453e827923913%26ts%3D1448627692%26attname%3DHTML5%E8%A7%86%E9%A2%91%E6%92%AD%E6%94%BE.apk)  
因此，我们参考了[Github上的一个库](https://github.com/cprcrack/VideoEnabledWebView)，做了自己的一部分优化，实际使用下来在魅族、华为、小米、三星、摩托、联想200块的手机上都可以正常播放，无崩溃。欢迎各位提建议  
已知问题：  
1. 华为p8原生浏览器上没有全屏按钮  
2. 由于退出全屏事件在很多机型上都会崩溃，因此为稳定性考虑，禁用了此事件，导致退出全屏后，当前视频是暂停状态的。

![输入图片说明](http://git.oschina.net/uploads/images/2015/1127/202425_f559980e_331643.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1127/202806_8974f225_331643.png "在这里输入图片标题")