package zhexian.app.softwebview;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;

/**
 * 视频播放demo
 * 注意要在AndroidManifest.xml里面加入硬件加速和旋转保存状态支持。
 * android:configChanges="keyboardHidden|orientation|screenSize"
 * android:hardwareAccelerated="true"
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String HTML_REPLACE_TAG = "{XLHtmlContent}";
    private String mHtmlPlaceHolder = "<html><head><title></title><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0, user-scalable=no\"/></head><body>" + HTML_REPLACE_TAG + "</body></html>";
    private String mTestContent = "<p>已知问题：\n" +
            "\n" +
            "华为P8没有全屏按钮。\n" +
            "\n" +
            "视频全屏返回之后，有个函数很多机型上调用会崩溃（测试下来只有moto、小米不会崩溃，魅族不崩溃，但是播放暂停不起效了。）因此禁用了该函数。\n" +
            "\n" +
            "导致全屏返回后，当前小的视频不能衔接全屏播放的进度。。</p><p><video controls=\"\" width=\"99%\"><source src=\"http://115.231.179.83/youku/6771D3B2B793282D86300D5676/030002030056582658D27E0247A48FE7D6B87D-5C71-41E6-4DF1-2A0C913AF3DE.flv\"></video><p>";

    /**
     * 退出视频全屏的按钮
     */
    private ImageView mImageCloseVideo;

    /**
     * 视频的容器
     */
    private ViewGroup mWebViewContainer;

    /**
     * 全屏的容器
     */
    private ViewGroup mFullScreenContainer;

    /**
     * 视频所在的WebView
     */
    private WebView mWebView;

    /**
     * 视频的Client，核心对象
     */
    private VideoWebChromeClient webChromeClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        bindData(mTestContent);
    }

    void initViews() {
        mImageCloseVideo = (ImageView) findViewById(R.id.videoFullScreen_close);
        mWebViewContainer = (ViewGroup) findViewById(R.id.video_container);
        mFullScreenContainer = (ViewGroup) findViewById(R.id.videoFullScreen_container);

        webChromeClient = new VideoWebChromeClient(mWebViewContainer, mFullScreenContainer, this);
        webChromeClient.setOnToggledFullscreen(new VideoWebChromeClient.ToggledFullscreenCallback() {
            @Override
            public void toggledFullscreen(boolean fullscreen) {
                mImageCloseVideo.setVisibility(fullscreen ? View.VISIBLE : View.GONE);

                if (fullscreen) {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                } else {
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });
        mWebView = new WebView(this);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(webChromeClient);
        mWebViewContainer.addView(mWebView);
        mImageCloseVideo.setOnClickListener(this);
    }

    void bindData(String text) {
        String newContent = new String(mHtmlPlaceHolder);
        mWebView.loadData(newContent.replace(HTML_REPLACE_TAG, text), "text/html; charset=UTF-8", null);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.videoFullScreen_close:
                webChromeClient.onBackPressed();
                break;
        }
    }
}
